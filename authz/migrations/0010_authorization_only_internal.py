# Generated by Django 4.1.9 on 2023-12-31 12:30

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("authz", "0009_alter_log_email_alter_log_ip_alter_log_params_and_more"),
    ]

    operations = [
        migrations.AddField(
            model_name="authorization",
            name="only_internal",
            field=models.BooleanField(
                default=True,
                help_text="Alleen zichtbaar binnen interne omgeving.",
                verbose_name="Alleen intern zichtbaar",
            ),
        ),
    ]
